# Welcome to MkDocs

This is a test site hosted on [GitLab Pages](https://pages.gitlab.io). You can
[browse its source code](https://gitlab.com/pages/mkdocs), fork it and start
using it on your projects.

For full documentation visit [mkdocs.org](http://mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout
[G-slides file](https://docs.google.com/presentation/d/1fdTmdepdaq03OSfcA3pYduDxDnQEyvY4ARPqXEX8KrY/edit#slide=id.g2823c3f9ca_0_9)  

    mkdocs.yml    # The configuration file.
    docs/Contact_Center_Best_Practices_WEBSITE_2.0.pdf
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
